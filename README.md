# @open-kappa/mywebserver

This project is a simple webserver based on Fastify.

## Links

* Project homepage: [hosted on GitLab Pages](
   https://open-kappa.gitlab.io/nodejs/mywebserver)
* Project sources: [hosted on gitlab.com](
   https://gitlab.com/open-kappa/nodejs/mywebserver)
* List of open-kappa projects, [hosted on GitLab Pages](
   https://open-kappa.gitlab.io)

## License

*@open-kappa/mywebserver* is released under the liberal MIT License.
Please refer to the LICENSE.txt project file for further details.

## Patrons

This node module has been sponsored by [Gizero Energie s.r.l.](
www.gizeroenergie.it).
