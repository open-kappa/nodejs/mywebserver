/* eslint-disable */

/**
 * @module -- Manual --
 */

export class Manual
{
    private constructor()
    {}

    /**
     * [[include:1.motivations.md]]
     */
    "1. Motivations": void;

    /**
     * [[include:2.contributing.md]]
     */
    "2. Contributing": void;

    /**
     * [[include:3.copyright.md]]
     */
    "3. Copyright": void;

    /**
     * [[include:4.changelog.md]]
     */
    "4. Changelog": void;
}
