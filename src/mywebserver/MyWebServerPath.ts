import type {
    FastifyReply,
    FastifyRequest
} from "fastify";
import type {HttpMethods} from "./HttpMethods";

/**
 * Type to specify URI instantations.
 */
export type MyWebServerPath = {
    /** The listening method. */
    method: HttpMethods;
    /** The URI path relative to the server base url. */
    path: string;
    /** The URI implementation. */
    callback: (req: FastifyRequest, res: FastifyReply) => Promise<void>;
};
