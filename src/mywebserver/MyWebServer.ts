import createFastify, {
    FastifyInstance,
    FastifyReply,
    FastifyRequest,
    FastifyServerOptions
} from "fastify";
import fastifyCors, {FastifyCorsOptions} from "fastify-cors";
import fastifyExpress from "fastify-express";
import path from "path";
import {
    buildMyWebServerOptions,
    type MyWebServerOptions
} from "./mywebserverImpl";
import qs from "qs";

/**
 * Converts the parameter to a boolean.
 * @param value - The parameter.
 * @returns The mathcing boolean value.
 * @throws In case of invalid string.
 */
function _toBool(value: boolean|string): boolean
{
    if (typeof value === "boolean") return value;
    return Boolean(JSON.parse(value));
}

function _hasOpt(name: string): boolean
{
    return process.argv.includes(name);
}

/**
 * Parse a CLI parameter.
 * Also remove the parameter from the argv list.
 * @param name - The parameter name.
 * @returns The parameter value.
 * @throws In case of missing parameter or missing parameter value.
 */
function _getOpt(name: string): string
{
    const index = process.argv.indexOf(name) + 1;
    if (index === 0)
    {
        throw new Error("Internal bug :(");
    }
    else if (index >= process.argv.length)
    {
        throw new Error(`Missing parameter for option ${name}`);
    }
    const ret = process.argv[index];
    process.argv.splice(index - 1, 2);
    return ret;
}

/**
 * Parse the environment options.
 * @returns The parsed options.
 */
function _parseEnvOptions(): Partial<MyWebServerOptions>
{
    const opts: Partial<MyWebServerOptions> = {};
    if (process.env.ADDRESS) opts.address = process.env.ADDRESS;
    if (process.env.LOGGER) opts.logger = _toBool(process.env.LOGGER);
    if (process.env.PORT) opts.port = Number(process.env.PORT);
    if (process.env.PREFIX) opts.prefix = process.env.PREFIX;
    if (process.env.TEST) opts.test = _toBool(process.env.TEST);
    return opts;
}

/**
 * Parse the CLI options.
 * @returns The parsed options.
 */
 function _parseCliOptions(): Partial<MyWebServerOptions>
 {
    const opts: Partial<MyWebServerOptions> = {};

    if (_hasOpt("-a")) opts.address = _getOpt("-a");
    else if (_hasOpt("--address")) opts.address = _getOpt("--address");

    if (_hasOpt("-l")) opts.logger = _toBool(_getOpt("-l"));
    else if (_hasOpt("--logger")) opts.logger = _toBool(_getOpt("--logger"));

    if (_hasOpt("-p")) opts.port = Number(_getOpt("-p"));
    else if (_hasOpt("--port")) opts.port = Number(_getOpt("--port"));

    if (_hasOpt("-x")) opts.prefix = _getOpt("-x");
    else if (_hasOpt("--prefix")) opts.prefix = _getOpt("--prefix");

    if (_hasOpt("-t")) opts.test = _toBool(_getOpt("-t"));
    else if (_hasOpt("--test")) opts.test = _toBool(_getOpt("--test"));

    return opts;
}

/**
 * The webserver class.
 */
export class MyWebServer
{
    /** Actual webserver instance. */
    private _fastify: FastifyInstance|null;
    /** Build options. */
    private _opts: MyWebServerOptions;

    /**
     * Constructor.
     * @param opts - The options.
     */
    public constructor(opts?: Partial<MyWebServerOptions>)
    {
        this._fastify = null;
        this._opts = {
            ...buildMyWebServerOptions(),
            ...opts,
            ..._parseEnvOptions(),
            ..._parseCliOptions()
        };
        console.log(JSON.stringify(this._opts, null, 4));
    }

    /**
     * Get the options.
     * @returns The options.
     */
    public get opts(): MyWebServerOptions
    {
        return this._opts;
    }

    /**
     * Start the server.
     */
    public async start(): Promise<void>
    {
        const self = this;
        async function listen(): Promise<string>
        {
            if (self._fastify === null)
            {
                return Promise.reject(new Error("Internal bug :("));
            }
            return self._fastify.listen(self._opts.port, self._opts.address);
        }
        return self._buildFastify()
            .then(() => self._registerRoutes())
            .then(listen)
            .then((address: string) => console.log(
                `Server is now listening on ${address}`
            ));
    }

    /**
     * Stop the server.
     */
    public async stop(): Promise<void>
    {
        const self = this;
        async function onClose(): Promise<void>
        {
            self._fastify = null;
            return Promise.resolve();
        }
        if (self._fastify === null) return Promise.resolve();
        return self._fastify.close()
            .then(onClose);
    }

    /**
     * Build the fastify instance.
     * @return a completion promise.
     */
    private async _buildFastify(): Promise<void>
    {
        const self = this;
        // @TODO helmet equivalent?
        const fastifyOptions: FastifyServerOptions = {
            "caseSensitive": true,
            "ignoreTrailingSlash": true,
            "logger": self._opts.logger,
            "querystringParser": (str) => qs.parse(str)
        };
        self._fastify = createFastify(fastifyOptions);
        // await fastify.register(middie);
        await self._registerCors();
        return self._fastify.register(fastifyExpress);
    }

    /**
     * Register the CORS.
     * @return A completion promise.
     */
    private async _registerCors(): Promise<void>
    {
        const self = this;
        type CorsOptionRegister = (
            req: FastifyRequest,
            callback: (err: Error|null, opts?: FastifyCorsOptions) => void
        ) => void;
        function corsOptionsRegister(
            req: FastifyRequest,
            callback: (err: Error|null, opts?: FastifyCorsOptions) => void
        ): void
        {
            const origin = req.headers.origin;
            // Do not include CORS headers for requests from localhost
            const corsOptions: FastifyCorsOptions = {
                "origin": !(origin && (/localhost/u).test(origin))
            };
            callback(null, corsOptions);
        }
        function buildOpts(_instance: FastifyInstance): CorsOptionRegister
        {
            return corsOptionsRegister;
        }
        if (self._fastify === null)
        {
            return Promise.reject(new Error("Internal bug :("));
        }
        return self._fastify.register(fastifyCors, buildOpts);
    }

    /**
     * Register the routs to listen for.
     * @returns A completion promise.
     */
    private async _registerRoutes(): Promise<void>
    {
        const self = this;
        if (self._fastify === null)
        {
            return Promise.reject(new Error("Internal bug :("));
        }
        console.log("Registering paths:");
        if (self._opts.test)
        {
            // eslint-disable-next-line no-inner-declarations
            async function test(
                _req: FastifyRequest,
                res: FastifyReply
            ): Promise<void>
            {
                res.type("application/json").code(200);
                res.send({"helloWorld": new Date().toISOString()});
                return Promise.resolve();
            }
            const url = path.join(self._opts.prefix, "test");
            console.log("- GET:", url);
            console.log("- POST:", url);
            self._fastify.get(url, test);
            self._fastify.post(url, test);
        }

        for (const pp of self._opts.paths)
        {
            const url = path.join(self._opts.prefix, pp.path);
            console.log(`- ${pp.method}: ${url}`);
            switch (pp.method)
            {
                case "GET":
                    self._fastify.get(
                        url,
                        pp.callback
                    );
                    break;
                case "HEAD":
                    self._fastify.head(
                        url,
                        pp.callback
                    );
                    break;
                case "POST":
                    self._fastify.post(
                        url,
                        pp.callback
                    );
                    break;
                case "PUT":
                    self._fastify.put(
                        url,
                        pp.callback
                    );
                    break;
                case "DELETE":
                    self._fastify.delete(
                        url,
                        pp.callback
                    );
                    break;
                case "CONNECT":
                    return Promise.reject(
                        new Error(`Unsupported HTTP method: ${pp.method}`)
                    );
                case "OPTIONS":
                    self._fastify.options(
                        url,
                        pp.callback
                    );
                    break;
                case "TRACE":
                    return Promise.reject(
                        new Error(`Unsupported HTTP method: ${pp.method}`)
                    );
                case "PATCH":
                    self._fastify.patch(
                        url,
                        pp.callback
                    );
                    break;
                case "ALL":
                    self._fastify.all(
                        url,
                        pp.callback
                    );
                    break;
                default:
                    return Promise.reject(
                        new Error(`Invalid HTTP method: ${pp.method}`)
                    );
            }
        }
        return Promise.resolve();
    }
}
