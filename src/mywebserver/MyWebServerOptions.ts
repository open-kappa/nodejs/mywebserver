import type {MyWebServerPath} from "./MyWebServerPath";

/**
 * Options for instantiating the webserver.
 */
export type MyWebServerOptions = {
    /** The address to serve. Default: "::" */
    address: string;
    /** Whether to log connections. Default: false. */
    logger: boolean;
    /** Array of paths to manage. Default is empty. */
    paths: Array<MyWebServerPath>;
    /** The port where to list. Default: 3000 */
    port: number;
    /** The serving prefix path. Default: "/" */
    prefix: string;
    /** Whether to expose the "/test" path. Default is false. */
    test: boolean;
};

export function buildMyWebServerOptions(): MyWebServerOptions
{
    return {
        "address": "::",
        "logger": false,
        "paths": [],
        "port": 3000,
        "prefix": "/",
        "test": false
    };
}
