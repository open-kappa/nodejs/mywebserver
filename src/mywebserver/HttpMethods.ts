/**
 * The list of possible HTTP methods.
 */
export type HttpMethods =
"GET"
|"HEAD"
|"POST"
|"PUT"
|"DELETE"
|"CONNECT"
|"OPTIONS"
|"TRACE"
|"PATCH"
|"ALL";
