import type {
    FastifyReply,
    FastifyRequest
} from "fastify";
import {
    MyWebServer,
    type MyWebServerOptions
} from "../mywebserver";

async function _testFail(_req: FastifyRequest, res: FastifyReply): Promise<void>
{
    const code = 418;
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    res.code(code);
    return Promise.reject(
        new Error("This must fail!")
    );
}

async function _onError(err: Error): Promise<void>
{
    // eslint-disable-next-line no-console
    console.error(err);
    return Promise.resolve();
}

// eslint-disable-next-line @typescript-eslint/naming-convention
async function main(): Promise<void>
{
    const opts: Partial<MyWebServerOptions> = {
        "logger": true,
        "paths": [
            {
                "callback": _testFail,
                "method": "GET",
                "path": "testFail"
            }
        ],
        "test": true
    };
    const server = new MyWebServer(opts);
    return server.start();
}

main()
    .catch(_onError);
