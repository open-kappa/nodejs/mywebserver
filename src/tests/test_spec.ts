/* eslint-disable class-methods-use-this */
import type {
    FastifyReply,
    FastifyRequest
} from "fastify";
import {
    MyWebServer,
    type MyWebServerOptions
} from "../mywebserver"
import {MyTest} from "@open-kappa/mytest";

class _ThisTest extends MyTest
{
    public constructor()
    {
        super("webserver test");
    }

    protected registerTests(): void
    {
        const self = this;
        self.registerTest(
            "Cli options parsing",
            self._testCliParsing.bind(self)
        );
        self.registerTest(
            "Env options parsing",
            self._testEnvParsing.bind(self)
        );
        self.registerTest(
            "Options precedence",
            self._testOptionsPrecedence.bind(self)
        );
        self.registerTest(
            "Server start",
            self._testServerStart.bind(self)
        );
        self.registerTest(
            "Server GET route",
            self._testServerGet.bind(self)
        );
        self.registerTest(
            "Server POST route",
            self._testServerPost.bind(self)
        );
    }

    private async _testCliParsing(): Promise<void>
    {
        const address = "1.2.3.4";
        const port = 1234;
        const prefix = "a/b/c/d";
        /* eslint-disable array-element-newline */
        process.argv.push(...[
            "-a", address,
            "--logger", "true",
            "-p", String(port),
            "-x", prefix,
            "--test", "true"
        ]);
        /* eslint-enable array-element-newline */
        const server = new MyWebServer();
        const opts = server.opts;
        const ok =
            opts.address === address
            && opts.logger
            && opts.port === port
            && opts.prefix === prefix
            && opts.test;
        if (!ok) return Promise.reject(new Error("Failed!"));
        return Promise.resolve();
    }

    private async _testEnvParsing(): Promise<void>
    {
        const address = "1.2.3.4";
        const port = 1234;
        const prefix = "a/b/c/d";
        process.env = {
            ...process.env,
            ...{
                "ADDRESS": address,
                "LOGGER": "true",
                "PORT": port,
                "PREFIX": prefix,
                "TEST": "true"
            }
        };
        const server = new MyWebServer();
        const opts = server.opts;
        const ok =
            opts.address === address
            && opts.logger
            && opts.port === port
            && opts.prefix === prefix
            && opts.test;
        if (!ok) return Promise.reject(new Error("Failed!"));
        return Promise.resolve();
    }

    private async _testOptionsPrecedence(): Promise<void>
    {
        const address = "1.2.3.4";
        const port = 1234;
        const prefix = "a/b/c/d";
        // - default < program < env < cli
        const progOpts: Partial<MyWebServerOptions> = {
            "address": "1.2.3.4",
            "logger": true
        };
        process.env = {
            ...process.env,
            ...{
                "LOGGER": "false",
                "PORT": "1234"
            }
        };
        process.argv.push(...[
            "-p", "4321",
            "-x", "a/b/c/d"
        ]);
        const server = new MyWebServer(progOpts);
        const opts = server.opts;
        const ok =
            opts.address === address
            && opts.logger
            && opts.port === port
            && opts.prefix === prefix
            && opts.test;
        if (!ok) return Promise.reject(new Error("Failed!"));
        return Promise.resolve();
    }

    private async _testServerStart(): Promise<void>
    {
        const server = new MyWebServer();
        return server.start()
            .then(() => server.stop());
    }

    private async _testServerGet(): Promise<void>
    {
        function cb(req: FastifyRequest, res: FastifyReply): Promise<void>
        {
            res.type("application/json").code(200);
            res.send({"hello": (req.query as any).value});
            return Promise.resolve();
        }
        const progOpts: Partial<MyWebServerOptions> = {
            "paths": [
                {
                    "callback": cb,
                    "method": "GET",
                    "path": "testGet"
                }
            ]
        };
        const server = new MyWebServer();
        async function checkResponse(res: Response): Promise<void>
        {
            res.json().then(
                (js) => js["hello"] === "world"
                    ? Promise.resolve()
                    : Promise.reject(new Error("Fetch failed"))
            )
        }
        async function doFetch(): Promise<Response>
        {
            const opts = server.opts;
            const url =
                `http://${opts.address}:${opts.port}/testGet?value=world`;
            return fetch(url,
                {
                    "headers": {
                        "Content-Type": "application/json",
                    },
                    "method": "GET"
                }
            );
        }

        return server.start()
            .then(doFetch)
            .then(checkResponse)
            .then(() => server.stop());
    }

    private async _testServerPost(): Promise<void>
    {
        return Promise.resolve();
    }
}

const _TEST = new _ThisTest();
_TEST.run();
